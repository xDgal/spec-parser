import json
import subprocess
import asyncio
#import sockjs
import threading
from queue import Queue

from bs4 import BeautifulSoup as bs
from aiohttp import web

ws_sessions = []
parsed_data = {}
data_ready = threading.Event()
data_ready.clear()

def index(request):
    return web.Response(text=json.dumps(parsed_data))

"""
async def ws_handler(msg, session):

    if msg.tp == sockjs.MSG_OPEN:
        pass

    elif msg.tp== sockjs.MSG_MESSAGE:
        if 'register' in msg.data:
            ws_sessions.append(session)
            session.send("ok")

    elif msg.tp== sockjs.MSG_CLOSED:
        session.send("Someone left.")
"""

async def websocket_handler(request):
    print('new connection')
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        print(msg)
        if msg.data == b'\x04\x00\x00\x00\x08\x00\x00\x00register':
            print('registering client', msg)
            ws_sessions.append(ws)

            global parsed_data
            if parsed_data:
                print('sending data')
                await ws.send_json(json.dumps(parsed_data))

    print('connection closed')
    ws_sessions.remove(ws)
    return ws

def pv(s):
    if '%' in s:
        return s.split(' ')[0]
    else:
        return s


def parseList(ll):
    plines = {}
    for l in ll:
        h = bs(l, 'html.parser')

        plname = h.find('h2', {'class': 'plname'}).text.strip()
        plines[plname] = {}
        plines[plname]['cunits'] = []


        # production line data
        tds = h.find('table', {'class': 'cunit-info'}).findAll('td')
        plines[plname]['partNo'] = tds[1].text.strip()
        plines[plname]['lotNo'] = tds[3].text.strip()
        plines[plname]['count'] = tds[5].text.strip()

        cunits = h.findAll('div', {'class': 'grid-container'})

        plines[plname]['quality'] = 1
        for cu in cunits:
            d = {}
            d['state'] = cu.find('div', {'class': 'msg'}).text.strip()
            d['since'] = cu.find('div', {'class': 'time'}).text.strip()
            d['reason'] = cu.find('div', {'class': 'reason'}).text.strip()
            d['name'] = cu.find('div', {'class': 'cu-name'}).text.strip()
            d['store'] = cu.find('div', {'class': 'store'}).text.strip()
            d['availability'] = pv(cu.find('div', {'class': 'avail'}).text.strip())
            d['performance'] = pv(cu.find('div', {'class': 'perf'}).text.strip())
            d['quality'] = pv(cu.find('div', {'class': 'qual'}).text.strip())

            # get quality for the whole production line
            try:
                plines[plname]['quality'] *= float(d['quality']) / 100
            except ValueError:
                pass

            plines[plname]['cunits'].append(d)

        plines[plname]['quality'] = str(int(plines[plname]['quality'] * 100))
        plines[plname]['availability'] = '-'
        plines[plname]['performance'] = '-'
        for cu in reversed(plines[plname]['cunits']):
            if cu['availability'] != '-' and cu['performance'] != '-':
                plines[plname]['availability'] = str(cu['availability'])
                plines[plname]['performance'] = str(cu['performance'])
                break

        try:
            pl = plines[plname]
            plines[plname]['oee'] = str(int(( float(pl['availability'])/100 *
                                              float(pl['quality'])/100 *
                                              float(pl['performance'])/100 ) * 100))
        except ValueError:
            plines[plname]['oee'] = '-'


    return plines


def parser():
    global parsed_data
    proc = subprocess.Popen(['nodejs', 'fetcher.js'], stdout=subprocess.PIPE)

    while True:
        out = proc.stdout.readline()
        parsed_data = parseList(eval(out))
        #print('data ready')
        data_ready.set()

async def dispatcher():
    global parsed_data
    while True:
        await asyncio.sleep(2)
        if data_ready.isSet():
            for s in ws_sessions:
                #s.send(json.dumps(parsed_data))
                await s.send_json(json.dumps(parsed_data))
            #print(parsed_data)
            data_ready.clear()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    app = web.Application(loop=loop)
    app.router.add_route('GET', '/', index)
    #sockjs.add_endpoint(app, ws_handler, prefix='/ws')

    app.add_routes([web.get('/ws', websocket_handler)])

    handler = app.make_handler()
    srv = loop.run_until_complete(loop.create_server(handler, '0.0.0.0', 8090))
    print('starting server ..')


    thread = threading.Thread(target=parser)
    thread.start()

    loop.create_task(dispatcher())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        srv.close()
        loop.run_until_complete(handler.finish_connections())
