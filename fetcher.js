const puppeteer = require('puppeteer');


function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};

(async () => {
  const browser = await puppeteer.launch(
      {args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  await page.goto('http://10.54.13.165/spec');
  while (true) {
    await page.reload();
    await timeout(15000)
    const bodyHandle = await page.$('body');
    let res = await page.$$eval('.pline', x => {
      return x.map(a => a.innerHTML)
    });
    console.log(JSON.stringify(res))
  }
  //await page.reload();
  await browser.close();
  await timeout(1000)
})();
