FROM ubuntu

COPY . /root/spec_parser

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt update \
  && apt -y install locales && locale-gen en_US.UTF-8 \
  && apt -y install nodejs npm python3 python3-pip chromium-browser \
  && pip3 install pipenv \
  && cd /root/spec_parser \
  && rm -rf node_modules && rm package-lock.json \
  && npm i puppeteer \
  && pipenv install --system --deploy

#fails
#RUN apt purge npm python3-pip && apt autoremove && apt clean

CMD cd /root/spec_parser && python3 parser.py
